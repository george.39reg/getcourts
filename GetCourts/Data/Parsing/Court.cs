﻿using Newtonsoft.Json;

namespace GetCourts.Data.Parsing
{
    public class Court
    {
        [JsonIgnore]
        public int RegionId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }
    }
}