﻿namespace GetCourts.Data.Parsing
{
    public class Region
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}