﻿using Newtonsoft.Json;

namespace GetCourts.Data.Parsing
{
    public class RegionCourts
    {
        public RegionCourts(Region region, IEnumerable<Court>? courts = null)
        {
            Region = region;
            Courts = courts?.ToList() ?? new List<Court>();
        }

        [JsonProperty("subject")]
        public Region Region { get; set; }

        [JsonProperty("child_courts")]
        public List<Court> Courts { get; set; }
    }
}