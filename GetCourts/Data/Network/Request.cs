﻿namespace GetCourts.Data.Network
{
    public class Request
    {
        public string httpMethod { get; set; }
        public Dictionary<string, string> headers { get; set; } = new Dictionary<string, string>();
        public string body { get; set; }
    }
}
