﻿using GetCourts.Authentication;
using GetCourts.Business;
using GetCourts.Data.Network;
using GetCourts.Extensions;
using Newtonsoft.Json;

namespace GetCourts.Handlers
{
    public class CourtDataHandler
    {
        public async Task<Response> FunctionHandler(Request request)
        {
            var authService = new AuthenticationService();
            
            var authResult = authService.Authorize(request.headers["Authorization"]);
            if (!authResult.Success)
            {
                var unauthorizedResponse = new ErrorResponse("Доступ запрещен");
                return new Response(403, JsonConvert.SerializeObject(unauthorizedResponse));
            }

            var courtDataParser = new CourtDataParser();
            var data = await courtDataParser.GetRegionsWithCourtsAsync();
            
            var strData = JsonConvert.SerializeObject(data);
            // YCloud has 3.5MB response size limit, so using compression to respect the limit
            var response = new Response(200, strData.Compress());
            response.isBase64Encoded = true;
            response.headers.Add("Content-Type", "application/json; charset=utf-8");
            response.headers.Add("Content-Encoding", "gzip");
            return response;
        }
    }
}
