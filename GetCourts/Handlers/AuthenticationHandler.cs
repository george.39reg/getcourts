﻿using GetCourts.Authentication;
using GetCourts.Data.Network;

namespace GetCourts.Handlers
{
    public class AuthenticationHandler
    {
        public AuthResponse FunctionHandler(Request request)
        {
            // couldn't find a way to customize authentication function response so moved authorization part to the main handler (CourtDataHandler)
            var authService = new AuthenticationService();
            var authResult = authService.Authorize(request.headers["Authorization"]);
            if (!authResult.Success)
            {
                return new AuthResponse() { isAuthorized = true };
            }

            return new AuthResponse();
        }
    }
}
