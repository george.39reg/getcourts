﻿namespace GetCourts.Authentication
{
    public class AuthorizationResult
    {
        public bool Success { get; set; }

        public string UserName { get; set; }
    }
}
