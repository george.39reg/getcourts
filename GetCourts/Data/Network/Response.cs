﻿namespace GetCourts.Data.Network
{
    public class Response
    {
        public Response(int statusCode, string body)
        {
            this.statusCode = statusCode;
            this.body = body;
        }

        public int statusCode { get; set; }

        public string body { get; set; }

        public Dictionary<string, string> headers { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, string> multiValueHeaders { get; set; } = new Dictionary<string, string>();

        public bool isBase64Encoded { get; set; }
    }
}
