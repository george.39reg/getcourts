﻿namespace GetCourts.Data.Network
{
    public class ErrorResponse
    {
        public ErrorResponse(string error)
        {
            this.error = error;
        }

        public string error { get; set; }
    }
}