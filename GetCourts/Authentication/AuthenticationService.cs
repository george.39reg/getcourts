﻿using System.Net.Http.Headers;
using System.Text;

namespace GetCourts.Authentication
{

    public class AuthenticationService
    {
        private const string DefaultUserName = "test";
        private const string DefaultPassword = "testpass";

        public AuthorizationResult Authorize(string authorization)
        {
            if (AuthenticationHeaderValue.TryParse(authorization, out var authHeader))
            {
#pragma warning disable CS8604 // Possible null reference argument.
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
#pragma warning restore CS8604 // Possible null reference argument.
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':', 2);
                var username = credentials[0];
                var password = credentials[1];
                if (username == DefaultUserName && password == DefaultPassword)
                {
                    return new AuthorizationResult() {  UserName = username };
                }
            }

            return new AuthorizationResult();
        }
    }
}
