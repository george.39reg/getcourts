using FluentAssertions;
using GetCourts.Business;
using NUnit.Framework;
using System.Threading.Tasks;

namespace GetCourts.Test
{
    public class CourtDataParserTests
    {
        [Test]
        public async Task GetRegionsWithCourtsAsync_Always_ReturnsData()
        {
            var courtDataParser = new CourtDataParser();
            var data = await courtDataParser.GetRegionsWithCourtsAsync();
            data.Should().HaveCountGreaterThan(0);
        }
    }
}