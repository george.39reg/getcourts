﻿namespace GetCourts.Handlers
{
    public class AuthResponse
    {
        public bool isAuthorized { get; set; }

        public object context { get; set; }
    }
}
