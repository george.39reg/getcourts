﻿using ByteDev.Strings;

namespace GetCourts.Data.Parsing
{
    public class ParsingResult<TData> where TData : class
    {
        public TData Data { get; set; }

        public string? Error { get; set; }

        public bool HasError => !Error.IsNullOrEmpty();
    }
}