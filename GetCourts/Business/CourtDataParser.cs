﻿using AngleSharp;
using AngleSharp.Io;
using ByteDev.Strings;
using GetCourts.Data.Parsing;
using Polly;

namespace GetCourts.Business
{
    public class CourtDataParser
    {
        public async Task<List<RegionCourts>> GetRegionsWithCourtsAsync()
        {
            string regionsDataUrl = "https://sudrf.ru/index.php?id=300&var=true#sp";
            string courtsDataUrl = "https://sudrf.ru/index.php?id=300&act=ajax_search&searchtype=sp&court_subj={0}&suds_subj=&var=true";

            var regionsExecutionPolicy = Policy
                    .HandleResult<ParsingResult<List<Region>>>(x => x.HasError)
                    .RetryAsync(3);

            var courtsExecutionPolicy = Policy
                    .HandleResult<ParsingResult<List<Court>>>(x => x.HasError)
                    .RetryAsync(3);

            var regions = await regionsExecutionPolicy.ExecuteAsync(() => GetRegionsAsync(regionsDataUrl));

            var courts = new List<Court>();
            foreach (var chunk in regions.Data.Chunk(20))
            {
                var rand = new Random(DateTime.UtcNow.Millisecond);

                var courtsData = await Task.WhenAll(chunk.Select(r => courtsExecutionPolicy.ExecuteAsync(() => GetCourtsAsync(string.Format(courtsDataUrl, r.Id), r.Id))));
                courts.AddRange(courtsData.SelectMany(x => x.Data));
                await Task.Delay(rand.Next(20, 50));
            }
            
            var result = regions.Data.GroupJoin(courts, x => x.Id, x => x.RegionId, (x, y) => new RegionCourts(x, y)).ToList();
            return result;
        }

        public async Task<ParsingResult<List<Region>>> GetRegionsAsync(string url)
        {
            var result = new ParsingResult<List<Region>>();
            var context = GetBrowsingContext();
            try 
            {
                var document = await context.OpenAsync(url);
                var regions = document
                    .QuerySelectorAll("[name='court_subj'] option")
                    .Where(x => !x.TextContent.IsNullOrWhitespace())
                    .Select(x => new Region()
                    {
                        Name = x.TextContent,
                        Id = x.GetAttribute("value")?.ToInt32() ?? 0
                    })
                    .ToList();

                result.Data = regions;
            }
            catch(Exception e)
            {
                result.Data = new List<Region>();
                result.Error = e.Message; 
            }

            return result;
        }

        public async Task<ParsingResult<List<Court>>> GetCourtsAsync(string url, int regionId)
        {
            var rand = new Random(DateTime.UtcNow.Millisecond);
            await Task.Delay(rand.Next(5, 10));
            var result = new ParsingResult<List<Court>>();
            var context = GetBrowsingContext();
            try 
            {
                var document = await context.OpenAsync(url);
                var courts = document
                    .QuerySelectorAll("option")
                    .Where(x => !x.TextContent.IsNullOrWhitespace())
                    .Select(x => new Court()
                    {
                        RegionId = regionId,
                        Name = x.TextContent,
                        Code = x.GetAttribute("value")?.ToString() ?? string.Empty
                    })
                    .ToList();

                result.Data = courts;
            }
            catch (Exception e)
            {
                result.Data = new List<Court>();
                result.Error = e.Message;
            }

            return result;
        }

        private static IBrowsingContext GetBrowsingContext()
        {
            // consider using user-agent rotation and proxies round-robin rotation
            var requester = new DefaultHttpRequester("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0");
            var config = Configuration.Default
                .WithDefaultLoader(new LoaderOptions() { IsResourceLoadingEnabled = true })
                .With(requester);

            var context = BrowsingContext.New(config);
            return context;
        }
    }
}